#include "qslidepanel.h"

QSlidePanel::QSlidePanel(QWidget *parent) : QWidget(parent)
{

  tabs = new QList<QSlidePanelTab*>();
  contents = new QList<QSlidePanelContent*>();
  tabsAnimation = new QList<QPropertyAnimation*>();
  contentsAnimation = new QList<QPropertyAnimation*>();
  parallelAnimation = new QParallelAnimationGroup(this);

  installEventFilter(this);

}

QSlidePanel::Direction QSlidePanel::direction() const
{
    return animDirection;
}

void QSlidePanel::setDirection(const Direction &align)
{
    animDirection = align;
}

int QSlidePanel::duration() const
{
  return animDuration;
}

void QSlidePanel::setDuration(int msecs)
{
  animDuration = msecs;
}

QEasingCurve QSlidePanel::easingCurve() const
{
  return animEasingCurve;
}

void QSlidePanel::setEasingCurve(const QEasingCurve &easing)
{
  animEasingCurve = easing;
}

QSlidePanel::OpenEvent QSlidePanel::openEvent() const
{
    return openEvt;
}

void QSlidePanel::setOpenEvent(const OpenEvent &event)
{
    openEvt = event;
}

bool QSlidePanel::isCloseable() const
{
  return isPanelCloseable;
}

void QSlidePanel::setCloseable(bool close)
{
  isPanelCloseable = close;
}

void QSlidePanel::addPanel(QSlidePanelTab *tab, QSlidePanelContent *content)
{

  // Set Tab widget
  tabs->append(tab);
  tab->setParent(this);
  tab->show();

  // Set Content widget
  contents->append(content);
  content->setParent(this);
  content->show();

  // Animation - Tab
  QPropertyAnimation *tabAnimation = new QPropertyAnimation(tab, "geometry");
  tabAnimation->setDuration(animDuration);
  tabAnimation->setEasingCurve(animEasingCurve);
  tabsAnimation->append(tabAnimation);

  // Animation - Content
  QPropertyAnimation *contentAnimation = new QPropertyAnimation(content, "geometry");
  contentAnimation->setDuration(animDuration);
  contentAnimation->setEasingCurve(animEasingCurve);
  contentsAnimation->append(contentAnimation);

  // Main Animation - One Animation to Control Them All
  parallelAnimation->addAnimation(tabAnimation);
  parallelAnimation->addAnimation(contentAnimation);

  tab->installEventFilter(this);

  animate(false);

}

void QSlidePanel::removePanel(int index)
{

  parallelAnimation->removeAnimation(tabsAnimation->at(index));
  parallelAnimation->removeAnimation(contentsAnimation->at(index));

  delete tabs->at(index);
  delete contents->at(index);
  tabs->removeAt(index);
  contents->removeAt(index);
  tabsAnimation->removeAt(index);
  contentsAnimation->removeAt(index);

  animate(false);

}

void QSlidePanel::openPanel(int index)
{
  emit closePanelEvent(tabOpenIndex);
  tabOpenIndex = index;
  animate();
  emit openPanelEvent(tabOpenIndex);
}

void QSlidePanel::closePanels()
{
  emit closePanelEvent(tabOpenIndex);
  tabOpenIndex = -1;
  animate();
}

bool QSlidePanel::eventFilter(QObject *object, QEvent *event)
{
  if (isOpenableEvent(event) && isTab(object)) {
      int index = tabs->indexOf(qobject_cast<QSlidePanelTab*>(object));
      if( index == tabOpenIndex && isCloseable() ) {
          closePanels();
        }
      else{
          openPanel(index);
        }
    }
  else if (event->type() == QEvent::Resize) {
      animate(false);
    }
  return false;
}

void QSlidePanel::paintEvent(QPaintEvent *)
{
  QStyleOption opt;
  opt.init(this);
  QPainter p(this);
  style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

bool QSlidePanel::isOpenableEvent(QEvent *event)
{
  bool mouseClick = event->type() == QMouseEvent::MouseButtonRelease && openEvt == MouseClick;
  bool mouseHover = event->type() == QMouseEvent::Enter && openEvt == MouseHover;
  return mouseClick || mouseHover;
}

bool QSlidePanel::isTab(QObject *object)
{
  return qobject_cast<QSlidePanelTab*>(object);
}

void QSlidePanel::animate(bool animation)
{

  int i = 0;
  QRect r;
  int contentSize = 0;

  QSlidePanelTab *tab = nullptr;
  QSlidePanelContent *content = nullptr;
  QPropertyAnimation *tabAnimation = nullptr;
  QPropertyAnimation *contentAnimation = nullptr;

  if( animDirection == Direction::LeftToRight || animDirection == Direction::RightToLeft ) {
      contentSize = this->width();
      for (i=0; i<tabs->size(); i++) {
          contentSize -= tabs->at(i)->maximumWidth();
        }
    } else {
      contentSize = this->height();
      for (i=0; i<tabs->size(); i++) {
          contentSize -= tabs->at(i)->maximumHeight();
        }
    }

  if( animDirection == Direction::LeftToRight ) {
      r = QRect(0,0,0,0);
    }
  else if( animDirection == Direction::RightToLeft ) {
      r = QRect(this->width(),0,0,0);
    }
  else if( animDirection == Direction::TopToBottom ) {
      r = QRect(0,0,0,0);
    }
  else if( animDirection == Direction::BottomToTop ) {
      r = QRect(0,this->height(),0,0);
    }

  for (int i=0; i<tabs->size(); i++) {

      tab = tabs->at(i);
      content = contents->at(i);
      tabAnimation = tabsAnimation->at(i);
      contentAnimation = contentsAnimation->at(i);

      // Tab
      if( animDirection == Direction::LeftToRight ) {
          r.setRect(r.x()+r.width(), r.y(), tab->maximumWidth(), this->height());
        }
      else if( animDirection == Direction::RightToLeft ) {
          r.setRect(r.x()-tab->maximumWidth(), r.y(), tab->maximumWidth(), this->height());
        }
      else if( animDirection == Direction::TopToBottom ) {
          r.setRect(r.x(), r.y()+r.height(), this->width(), tab->maximumHeight());
        }
      else if( animDirection == Direction::BottomToTop ) {
          r.setRect(r.x(), r.y()-tab->maximumHeight(), this->width(), tab->maximumHeight());
        }
      if(animation){
          tabAnimation->setStartValue(QRect(tab->x(),tab->y(),tab->width(),tab->height()));
          tabAnimation->setEndValue(r);
        }
      else{
          tab->setGeometry(r);
        }

      // Content
      if( animDirection == Direction::LeftToRight ) {
          r.setRect(r.x()+r.width(), r.y(), (i == tabOpenIndex ? contentSize : 0), r.height());
        }
      else if( animDirection == Direction::RightToLeft ) {
          r.setRect(r.x()-(i == tabOpenIndex ? contentSize : 0), r.y(), (i == tabOpenIndex ? contentSize : 0), r.height());
        }
      else if( animDirection == Direction::TopToBottom ) {
          r.setRect(r.x(), r.y()+r.height(), r.width(), (i == tabOpenIndex ? contentSize : 0));
        }
      else if( animDirection == Direction::BottomToTop ) {
          r.setRect(r.x(), r.y()-(i == tabOpenIndex ? contentSize : 0), r.width(), (i == tabOpenIndex ? contentSize : 0));
        }
      if(animation){
          contentAnimation->setStartValue(QRect(content->x(),content->y(),content->width(),content->height()));
          contentAnimation->setEndValue(r);
        }
      else{
          content->setGeometry(r);
        }

    }

  parallelAnimation->stop();
  if( animation ) {
      parallelAnimation->start();
    }

}
