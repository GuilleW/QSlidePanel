#ifndef QSLIDEPANELCONTENT_H
#define QSLIDEPANELCONTENT_H

#include <QPainter>
#include <QStyleOption>
#include <QWidget>

/**
 * @brief The QSlidePanelContent class is just used to easily find Content Widget in style sheet.
 */
class QSlidePanelContent : public QWidget
{
  Q_OBJECT

public:
  /**
   * @brief The content panel widget.
   * @param parent
   */
  explicit QSlidePanelContent(QWidget *parent = nullptr);

protected:
  /**
   * @brief Reimplement paintEvent to use full power of style sheet.
   */
  void paintEvent(QPaintEvent *) override
   {
       QStyleOption opt;
       opt.init(this);
       QPainter p(this);
       style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
   }

};

#endif // QSLIDEPANELCONTENT_H
