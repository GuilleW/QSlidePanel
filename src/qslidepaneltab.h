#ifndef QSLIDEPANELTAB_H
#define QSLIDEPANELTAB_H

#include <QPainter>
#include <QStyleOption>
#include <QWidget>

/**
 * @brief The QSlidePanelTab class is just used to easily find Tab Widget in style sheet.
 */
class QSlidePanelTab : public QWidget
{
  Q_OBJECT

public:
  /**
   * @brief The tab panel widget.
   * @param parent
   */
  explicit QSlidePanelTab(QWidget *parent = nullptr);

protected:
  /**
   * @brief Reimplement paintEvent to use full power of style sheet.
   */
  void paintEvent(QPaintEvent *) override
   {
       QStyleOption opt;
       opt.init(this);
       QPainter p(this);
       style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
   }

};

#endif // QSLIDEPANELTAB_H
