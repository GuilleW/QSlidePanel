#ifndef QSLIDEPANEL_H
#define QSLIDEPANEL_H

#include <QList>
#include <QPainter>
#include <QParallelAnimationGroup>
#include <QPropertyAnimation>
#include <QStyleOption>

#include "qslidepanelcontent.h"
#include "qslidepaneltab.h"

/**
 * @brief The QSlidePanel widget is a Qt class that allows elements to expand and collapse creating a sliding panel effect.
 *
 * ⚙️ Default configuration is:
 *   - QSlidePanel::direction : Direction::TopToBottom
 *   - QSlidePanel::duration : 500 ms
 *   - QSlidePanel::easingCurve : [QEasingCurve::OutQuart](https://doc.qt.io/qt-5/qeasingcurve.html#Type-enum)
 *   - QSlidePanel::openEvent : OpenEvent::MouseClick
 *   - QSlidePanel::isCloseable : False
 *
 * 📐 If QSlidePanel is a :
 *    - vertical based widget, QSlidePanelTab should have the [maximumHeight](http://doc.qt.io/qt-5/qwidget.html#maximumHeight-prop) property set (from method or stylesheet).
 *    - horizontal based widget, QSlidePanelTab should have the [maximumWidth](http://doc.qt.io/qt-5/qwidget.html#maximumWidth-prop) property set (from method or stylesheet).
 *
 * Also, QSlidePanel, QSlidePanelTab and QSlidePanelContent override [QWidget::paintEvent](http://doc.qt.io/qt-5/qwidget.html#paintEvent) to unleash the full power of StyleSheet 🚀.
 */
class QSlidePanel : public QWidget
{
  Q_OBJECT

public:

  /**
   * @brief Alignement of each panels start from one side to the other side.
   */
  enum  Direction { LeftToRight, RightToLeft,  TopToBottom,  BottomToTop };
  Q_ENUMS(Direction)

  /**
   * @brief Panels open from click or Hover on Tab.
   */
  enum  OpenEvent { MouseClick, MouseHover };
  Q_ENUMS(OpenEvent)

  /**
   * @brief QSlidePanel constructor. It's a simple QWidget.
   * @param parent
   */
  explicit QSlidePanel(QWidget *parent = nullptr);

  /**
   * @brief Return the alignement direction.
   * @return QSlidePanel::Direction
   */
  Direction direction() const;
  /**
   * @brief Configure the alignement of each panels from one side to the other side.
   * @param align Alignement of panels.
   */
  void setDirection(const Direction &align);
  /**
   * @brief Return the animation duration on open/close.
   * @return int
   */
  int duration() const;
  /**
   * @brief Configure the animation duration on open/close.
   * @param msecs Animation duration in millisecond.
   */
  void setDuration(int msecs);
  /**
   * @brief Return the easing curves that control the animation.
   * @return [QEasingCurve](http://doc.qt.io/qt-5/qeasingcurve.html)
   */
  QEasingCurve easingCurve() const;
  /**
   * @brief Configure the easing curves for controlling animation.
   * @param easing A [QEasingCurve](http://doc.qt.io/qt-5/qeasingcurve.html) transition
   */
  void setEasingCurve(const QEasingCurve &easing);
  /**
   * @brief Return the open event.
   * @return QSlidePanel::OpenEvent
   */
  OpenEvent openEvent() const;
  /**
   * @brief Configure the open event: on click or on hover.
   * @param event QSlidePanel::OpenEvent
   */
  void setOpenEvent(const OpenEvent &event);
  /**
   * @brief Return if panels ca be closed by user.
   * @return bool
   */
  bool isCloseable() const;
  /**
   * @brief Configure if panels can be closed without opening another panel. Close event is the same as Open event: Click or Hover on tab.
   * @param close Set it to true if user can close panels.
   */
  void setCloseable(bool close);

  /**
   * @brief Add a QSlidePanelTab and a QSlidePanelContent in the QSlidePanel you have already instanciated and, of course, designed.
   *        QSlidePanelTab and QSlidePanelContent are simple [QWidget](http://doc.qt.io/qt-5/qwidget.html).
   *
   * @param tab A QSlidePanelTab widget
   * @param content A QSlidePanelContent widget
   */
  void addPanel(QSlidePanelTab *tab, QSlidePanelContent *content);
  /**
   * @brief Remove QSlidePanelTab and QSlidePanelContent based on index.
   * @param index Index of first QSlidePanelTab and QSlidePanelContent start at 0!
   */
  void removePanel(int index);
  /**
   * @brief Open QSlidePanelContent based on index.
   *        It emit openPanelEvent signal with tab opened index.
   * @param index Index of first QSlidePanelContent start at 0!
   */
  void openPanel(int index);
  /**
   * @brief Close opened QSlidePanelContent. We don't need index since we can't open two panels at same time.
   *        It emit closePanelEvent signal with tab closed index.
   */
  void closePanels();

signals:
  /**
   * @brief When openPanel() is used, emit signal to openPanelEvent() with index of opened panel.
   * @param index Index of panel opened
   */
  void openPanelEvent(int index);
  /**
   * @brief When closePanels() is used, emit signal to closePanelEvent() with index of closed panel.
   * @param index Index of panel closed
   */
  void closePanelEvent(int index);

public slots:

protected:
  /**
   * @brief Manage events like, click, mouse enter, and resize widget.
   * @param object wich receive event
   * @param event type
   * @return
   */
  bool eventFilter(QObject *object, QEvent *event) override;
  /**
   * @brief Reimplement paintEvent to use full power of style sheet.
   */
  void paintEvent(QPaintEvent *) override;

private:
  int tabOpenIndex = -1;
  Direction animDirection = TopToBottom;
  int animDuration = 500;
  QEasingCurve animEasingCurve = QEasingCurve::OutQuart;
  OpenEvent openEvt = MouseClick;
  bool isPanelCloseable = false;
  QList<QSlidePanelTab*> *tabs = nullptr;
  QList<QSlidePanelContent*> *contents = nullptr;
  QList<QPropertyAnimation*> *tabsAnimation = nullptr;
  QList<QPropertyAnimation*> *contentsAnimation = nullptr;
  QParallelAnimationGroup *parallelAnimation = nullptr;
  bool isOpenableEvent(QEvent *event);
  bool isTab(QObject *object);
  void animate(bool animation = true);
};

#endif // QSLIDEPANEL_H
