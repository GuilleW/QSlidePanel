# QSlidePanel

A [Qt](https://qt.io) SlidePanel widget that allows elements to expand and collapse creating a sliding panel effect.


Read [the doc](./html/docs/index.html) for more details about the QSlidePanel methods.

## Install

Open a shell in your Qt project :

```sh
mkdir vendor
cd vendor
git clone https://gitlab.com/GuilleW/QSlidePanel.git

```

## Example


```cpp
#include "vendor/QSlidePanel/src/qslidepanel.h"

  // Style example
  qApp->setStyleSheet(
    "QSlidePanel { background:white; }"
    "QSlidePanel QSlidePanelTab#Tab1 { background:rgba(100,0,0,0.5); max-height:20px;}"
    "QSlidePanel QSlidePanelTab#Tab2 { background:rgba(0,100,0,0.5); max-height:40px;}"
    "QSlidePanel QSlidePanelTab#Tab3 { background:rgba(0,0,100,0.5); max-height:60px;}"
    "QSlidePanel QSlidePanelContent {background:rgba(100,100,100,1);}"
    );


  // Create Slide Panel
  QSlidePanel *slidePanel = new QSlidePanel();
  slidePanel->resize(300,300);
  slidePanel->show();
  QSlidePanelTab *tab = nullptr;
  QSlidePanelContent *content = nullptr;

  // Options : This is the default values
  slidePanel->setDirection(QSlidePanel::TopToBottom);
  slidePanel->setDuration(500);
  slidePanel->setEasingCurve(QEasingCurve::OutQuart);
  slidePanel->setOpenEvent(QSlidePanel::MouseClick);
  slidePanel->setCloseable(false);

  // First Panel
  tab = new QSlidePanelTab;
  tab->setObjectName("Tab1");
  content = new QSlidePanelContent;
  slidePanel->addPanel(tab, content);

  // Second Panel
  tab = new QSlidePanelTab;
  tab->setObjectName("Tab2");
  content = new QSlidePanelContent;
  slidePanel->addPanel(tab, content);

  // Third Panel
  tab = new QSlidePanelTab;
  tab->setObjectName("Tab3");
  content = new QSlidePanelContent;
  slidePanel->addPanel(tab, content);

  // Open Second panel (First start at 0)
  slidePanel->openPanel(1);

```

## Contribute
All contributions, code, feedback and strategic advice, are welcome. If you have a question you can contact me directly via email or simply [open an issue](https://gitlab.com/GuilleW/QSlidePanel/issues/new) on the repository. I'm also always happy for people helping me test new features.
